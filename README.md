# Blandpkg

Blandpkg is Slackware's **slackpkg** suite ported for non-Slack distributions. You should not use it, you should use <a href="http://asic-linux.com.mx/~izto/checkinstall/index.php" target="_top">checkinstall</a>.


## Is it useful?

Currently, I am using it on Debian, and it allows me to install packages for which I have Slackbuilds but no Debian packages. It works; shrug.

I have *not* ported **upgradepkg** yet. Eventually, I might.


## Blandpkg vs. Checkinstall

No contest. Use <a href="http://asic-linux.com.mx/~izto/checkinstall/index.php" target="_top">Checkinstall</a>. Checkinstall is a finely-tuned and expertly written packaging system that, frankly, beats most other solutions out there.

Blandpkg only exists because I am so heavily invested in Slackware that it's just easier for me to have it on non-Slack distributions.